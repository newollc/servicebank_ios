//
//  PushNotificationManager.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/19/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications

class PushNotificationManager: NSObject {
    
    static var shared = PushNotificationManager()
    let center = UNUserNotificationCenter.current()
    var options: UNAuthorizationOptions?
    
    func requestAuthorization(application: UIApplication) {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 12.0, *) {
            options = [.provisional, .alert, .sound, .badge]
        } else {
            options = [.alert, .sound, .badge]
        }
        
        center.requestAuthorization(options: options!) { (isGranted, err) in
            if err != nil {
                //Something bad happend
            } else {
                print("Authorized")
                self.center.delegate = self
            }
        }
    }
    
    func sendLocalPush(title: String?, body: String?, subtitle: String?) {
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        if let title =  title {
            content.title = title
        }
        
        if let subtitle =  subtitle {
               content.subtitle = subtitle
        }
        if let body =  body {
            content.body = body
        }
        content.badge = 1
        
        let request = UNNotificationRequest(identifier: Constant.NotificationKey.KEY_SERVICEBANK_NOTIFICATION, content: content, trigger: nil)
        
        center.add(request, withCompletionHandler: nil)
        
        UIApplication.shared.applicationIconBadgeNumber += content.badge as! Int
    }
    
    func removeNotificationBadges() {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func getUrlFromText(notificationBody: String) {
        let input = notificationBody
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            DataManagerHelper.setUrl(url: String(url))
        }
    }
}

extension PushNotificationManager: UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        print("Message ID 1")
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        print("Message ID 2")
        
        // Print full message.
        print(userInfo)
        
        if let url = userInfo[Constant.NotificationKey.KEY_SERVICEBANK_NOTIFICATION_URL] as? NSString {
            getUrlFromText(notificationBody: url as String)
        }
        
        completionHandler()
    }
    
}

extension PushNotificationManager: MessagingDelegate {
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("Test")
//        let notificationData: [String : Any] = remoteMessage.appData["notification"] as! [String : Any]
//        guard let body = notificationData[Constant.NotificationKey.KEY_SERVICEBANK_NOTIFICATION_BODY] as? String else { return }
//        let title: String? = notificationData[Constant.NotificationKey.KEY_SERVICEBANK_NOTIFICATION_TITLE] as? String
//        let url: String? = notificationData[Constant.NotificationKey.KEY_SERVICEBANK_NOTIFICATION_URL] as? String
//        sendLocalPush(title: title, body: body, subtitle: "")
//    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        ConnectToFCM()
    }
}

extension PushNotificationManager {
    
    func ConnectToFCM() {
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("DCS: \(result.token)")
                UserDefaultHelper.setToken(token: result.token)
            }
        }
    }
    
    func disableDirectChannel() {
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
}
