//
//  UserDefaultsHelper.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/20/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import Foundation

class UserDefaultHelper {
    
    class func setToken(token: String) {
        UserDefaults.standard.set(token, forKey: Constant.UserDefaultKey.KEY_NOTIFICATION_TOKEN)
    }
    
    class func getToken() -> String? {
        return UserDefaults.standard.string(forKey: Constant.UserDefaultKey.KEY_NOTIFICATION_TOKEN)
    }
}
