//
//  DataManager.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 11/26/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import Foundation
import UIKit

class DataManagerHelper {
    
    private static var url: String = Constant.Url.URL_APP_BASE
    
    static func setUrl(url: String) {
        self.url = url
        NotificationCenter.default.post(
            name: UIApplication.newUrlFromNotification,
            object: nil,
            userInfo: nil)
    }
    
    static func getUrl() -> String {
        return self.url
    }
}
