//
//  Authentication.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/20/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import Foundation

struct Authentication: Decodable {
    
    let messages: Array<String>
    let loggedIn: Bool
    
    enum CodingKeys : String, CodingKey {
        case messages
        case loggedIn = "logged_in"
    }
}
