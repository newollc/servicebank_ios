//
//  ViewController.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 8/24/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import UIKit
import WebKit

class HomeViewController: UIViewController {

    @IBOutlet weak var viewSplash: UIView!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setUi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBtnRefresh(_ sender: Any) {
        self.webView.reload()
    }
    
    deinit {
        // Without this, it'll crash when your MyClass instance is deinit'd
        self.webView.scrollView.delegate = nil
    }
    
}

extension HomeViewController {
    
    func setUi() {
        setWebView()
        subscribe()
    }
    
    func setWebView() {
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: Constant.Url.URL_APP_BASE)!))
    }
    
    func hideSplash() {
        Timer.scheduledTimer(withTimeInterval: Constant.DefaultConfig.SPLASH_AFTER_LOADING_WAITING, repeats: false) { (timer) in
            self.viewSplash.isHidden = true
        }
    }
    
    func subscribe() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(urlDidUpdate(_:)),
                                               name: UIApplication.newUrlFromNotification,
                                               object: nil)
    }
    
    @objc func urlDidUpdate(_ notification: Notification) {
        self.webView.loadRequest(URLRequest(url: URL(string: DataManagerHelper.getUrl())!))
    }
    
}

extension HomeViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        guard let url = request.url else { return true }
        if !url.absoluteString.contains(Constant.Url.URL_APP_BASE) {
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            return false
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        if NetworkConnection.isConnectedToNetwork() {
            self.webView.isHidden = false
        } else {
            self.webView.isHidden = true
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("WebPageFinished")
        hideSplash()
        ApiRequestHandler.shared.isUserAuthenticated()
    }
    
}



