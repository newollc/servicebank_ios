//
//  Constant.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/17/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import Foundation

struct Constant {
    
    struct DefaultConfig {
        static let SPLASH_AFTER_LOADING_WAITING = 1.0
    }
    
    struct Url {
        static let URL_APP_BASE: String = "https://app.servicebank.com"
        static let URL_GET_AUTH = "https://app.servicebank.com/verify-auth"
        static let URL_SET_TOKEN = "https://app.servicebank.com/firebase-token"
        
        static let URL_KEY_TOKEN = "token"
    }
    
    struct UserDefaultKey {
        static let KEY_NOTIFICATION_TOKEN: String = "notification_token"
    }
    
    struct NotificationKey {
        static let KEY_SERVICEBANK_NOTIFICATION = "ServiceBankNotification"
        static let KEY_SERVICEBANK_NOTIFICATION_BODY = "body"
        static let KEY_SERVICEBANK_NOTIFICATION_TITLE = "title"
        static let KEY_SERVICEBANK_NOTIFICATION_URL = "url"
        
    }
}
