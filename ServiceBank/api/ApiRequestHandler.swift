//
//  ApiRequestHandler.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/20/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import Foundation

class ApiRequestHandler: NSObject {
    
    static let shared = ApiRequestHandler()
    
    fileprivate let config: URLSessionConfiguration
    fileprivate let session: URLSession
    
    override init() {
        config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
    }
    
    func isUserAuthenticated() {
        let url = URL(string: Constant.Url.URL_GET_AUTH)
        print("Request sent")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        let task = session.dataTask(with: request) { (data, response, error) in
            // ensure there is no error for this HTTP response
            guard error == nil else {
                print ("error: \(error!)")
                return
            }
            
            // ensure there is data returned from this HTTP response
            guard let content = data else {
                print("No data")
                return
            }
            
            // parse the json to the object
            do {
                let item = try JSONDecoder().decode(Authentication.self, from: content)
                print("token error \(item)")
                if item.loggedIn {
                    guard let token = UserDefaultHelper.getToken() else { return }
                    print("token \(token)")
                    self.sendNotificationToken(token: token)
                }
            } catch let jsonErr {
                print("Error serializing json",  jsonErr)
            }

        }
        task.resume()
    }
    
    func sendNotificationToken(token: String) {
        let url = URL(string: Constant.Url.URL_SET_TOKEN)
        let body = "token=\(token)"
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = body.data(using: String.Encoding.utf8)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            // ensure there is no error for this HTTP response
            guard error == nil else {
                print ("error: \(error!)")
                return
            }
            
            // ensure there is data returned from this HTTP response
            guard let content = data else {
                print("No data")
                return
            }
            
            print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
        }
        task.resume()
    }
    
}
