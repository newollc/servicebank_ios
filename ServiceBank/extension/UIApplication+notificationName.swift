//
//  UIApplication+notificationName.swift
//  ServiceBank
//
//  Created by Rahman Mammadov on 9/19/18.
//  Copyright © 2018 NewoLab. All rights reserved.
//

import UIKit

extension UIApplication {
    
    static let notificationName = Notification.Name("yourCustomNotificationName")
    static let newUrlFromNotification = Notification.Name("newUrlFromNotification")
}
